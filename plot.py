from ROOT import *

hist = TH1D("hist","hist",4096,0.0,4096.0)

for line in open("test.dat","r"):
    vals = line.split(" ")
    if len(vals) == 0: continue
    #print vals
    if vals[0] != "EVENT": continue
    print float(vals[4])
    hist.Fill( float(vals[4]))

hist.Draw("HIST")
gPad.SetLogy(1)
gPad.Update()
raw_input("WAIT")
            
