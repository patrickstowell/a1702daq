// Mainframe macro generated from application: /home/home/root-6.02.02/bin/root.exe
// By ROOT version 6.02/02 on 2015-05-17 23:03:49
#include "TBenchmark.h"
#include "TSystem.h"
#include "TGraph.h"
#include "TH1F.h"
#include "TH2F.h"

#include "FEBDTP.hxx"
#include "febdrv.h"
#include <stdint.h>

#include "TObject.h"
#include <net/if.h>
#include "Riostream.h"
#include "TMath.h"
#include "TF1.h"
#include "TTree.h"
#include "time.h"
#include <sys/timeb.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>

bool ConfigGetBit(UChar_t *buffer, UShort_t bitlen, UShort_t bit_index)
{
  UChar_t byte;
  UChar_t mask;
  byte = buffer[(bitlen - 1 - bit_index) / 8];
  mask = 1 << (7 - bit_index % 8);
  byte = byte & mask;
  if (byte != 0) return true; else return false;
}

// Global Functions
int Init(const char* iface);
void HVON();
void HVOF();
void StartDAQ(int nev);
void StopDAQ();
void SendConfig();
void FillHistos(int truncat);
UInt_t GrayToBin(UInt_t n);
Double_t mppc0( Double_t *xx, Double_t *par);
Double_t mppc1( Double_t *xx, Double_t *par);
void ConfigSetBit(UChar_t *buffer, UShort_t bitlen, UShort_t bit_index, Bool_t value);

void SetThresholdDAC1(UShort_t dac1);
UShort_t GetThresholdDAC1();
void SetThresholdDAC2(UShort_t dac2);
float GetTriggerRate();
void UpdateConfig();
void UpdateHisto();
void UpdateBoardMonitor();
void DAQ(int nev);
UChar_t ConfigGetGain(int chan);
UChar_t ConfigGetBias(int chan);
void ConfigSetGain(int chan, UChar_t val);
void ConfigSetBias(int chan, UChar_t val);
void SetDstMacByIndex(int i);
  

// Global Variables
FEBDTP* t; // Socket Object
UChar_t buf[1500]; // Main Buffer

long int ts0_ref_AVE[256]; //Average value for PPS ts0, used for VCXO feedback
Int_t ts0_ref_IND[256]; //Number of averaged values for PPS ts0, used for VCXO feedback
UInt_t overwritten = 0; // Overwritten events
UInt_t lostinfpga = 0;  // Lost events
UInt_t total_lost = 0;  // Total lost

UShort_t chg[32];
UInt_t ts0, ts1;
UInt_t ts0_ref, ts1_ref;
UInt_t ts0_ref_MEM[256], ts1_ref_MEM[256]; //memorized time stamps for all febs
Bool_t NOts0 = false, NOts1 = false;
UInt_t ts0_ref_mon, ts1_ref_mon;
Bool_t NOts0_mon = false, NOts1_mon = false;


#define maxpe 10 //max number of photoelectrons to use in the fit
int NEVDISP = 200; //number of lines in the waterfall event display
const Double_t initpar0[7] = {7000, 100, 700, 9.6, 1.18, 0.3, 0.5};
const Double_t initpar1[7] = {3470, 100, 700, 9.5, 2.25, 3e-3, 3.7e-2};
Double_t peaks[maxpe]; //positions of peaks in ADC counts
Double_t peaksint[maxpe]; //expected integral of each p.e. peak
UShort_t VCXO_Value = 500; //DAC settings to correct onboard VCTCXO
UShort_t VCXO_Values[256]; //DAC settings to correct onboard VCTCXO per board, index=mac5
TF1* f0;
TF1* f1;
UChar_t mac5 = 0x00;
UChar_t bufPMR[1500];
UChar_t bufSCR[1500];
int evs = 0; //overall events per DAQ
// int evs_notfirst=0; //overall events per DAQ without very first request
int evsperrequest = 0;
Int_t chan = 0; //channel to display on separate canvas c1
Int_t BoardToMon = 0; //board to display on separate canvas c1
int RunOn = 0;

time_t tm0, tm1;

uint32_t CHAN_MASK = 0xFFFFFFFF; // by default all channels are recorded

int fChanProbe[33];
int fChanEnaAmp[34];
int fChanEnaTrig[33];
int fChanGain[32];
int fChanBias[32];

TFile* outputfile;
TTree* outputtree;

int DACTHRESHOLD = 250;

// Main Analysis Code
int main(int argv, const char argc)
{
	const char *iface = "enx00e04c680922";
	if (Init(iface) == 0) return 1;

	// Update the Config
	UpdateConfig();
	SendConfig();

	printf("Turning HV on\n");
	HVON();
	printf("Starting DAQ\n");
	StartDAQ(100000);
//	sleep(20);
	printf("Turning HV off \n");
	HVOF();

	return 0;
}

int Init(const char *iface = "eth1")
{
	t = new FEBDTP(iface);
	if (t->ScanClients() == 0) {printf("No clients connected, exiting\n"); return 0;};
	t->PrintMacTable();

	t->VCXO = 500;
	for (int feb = 0; feb < t->nclients; feb++)  { ts0_ref_AVE[t->macs[feb][5]] = 0; ts0_ref_IND[t->macs[feb][5]] = 0; }

	t->setPacketHandler(&FillHistos);
	return 1;
}


void UpdateConfig()
{
	char bsname[32];
	uint8_t bufFIL[256];

	t->ReadBitStream("CITIROC_PROBEbitstream.txt", bufPMR);
	for (int feb = 0; feb < t->nclients; feb++)
	{
		SetDstMacByIndex(feb);
		sprintf(bsname, "CITIROC_SC_SN%03d.txt", t->dstmac[5]);
		if (!(t->ReadBitStream(bsname, bufSCR))) t->ReadBitStream("CITIROC_SC_PROFILE1.txt", bufSCR);

		*((uint32_t*)(&(bufFIL[0]))) = *((uint32_t*)(&(bufSCR[265]))); //copy trigger enable channels from SCR to FIL tregister

		t->SendCMD(t->dstmac, FEB_SET_RECV, DACTHRESHOLD, t->srcmac);
		t->SendCMD(t->dstmac, FEB_WR_SCR, 0x0000, bufSCR);
		t->SendCMD(t->dstmac, FEB_WR_PMR, 0x0000, bufPMR);
		t->SendCMD(t->dstmac, FEB_WR_FIL, 0x0000, bufFIL);

		if (feb == BoardToMon)  {
			for (int i = 265; i < 265 + 32; i++)
					if (ConfigGetBit(bufSCR, 1144, i)) fChanEnaTrig[i - 265] = 1; else fChanEnaTrig[i - 265] = 0;
			if (ConfigGetBit(bufSCR, 1144, 1139)) fChanEnaTrig[32] = 1; else fChanEnaTrig[32] = 1;
			for (int i = 0; i < 32; i++)
					if (ConfigGetBit(bufSCR, 1144, 633 + i * 15)) fChanEnaAmp[i] = 0; else fChanEnaAmp[i] = 1;
			fChanEnaAmp[33] = 0; fChanEnaAmp[32] = 0;
			for (int i = 0; i < 32; i++)
					if (ConfigGetBit(bufPMR, 224, 96 + i)) fChanProbe[i] = 1; else fChanProbe[i] = 0;
			DACTHRESHOLD = GetThresholdDAC1(); // fNumberEntry755->SetNumber(GetThresholdDAC1());
			for (int i = 0; i < 32; i++) fChanGain[i] = (ConfigGetGain(i));
			for (int i = 0; i < 32; i++) fChanBias[i] = (ConfigGetBias(i));

		}
	}
	for (int i = 0; i < 32; i++){
	  printf("E T P G B %d %d %d %d %d \n",fChanEnaAmp[i], fChanEnaTrig[i], fChanProbe[i], fChanGain[i], fChanBias[i]);
	}
        printf("OR 32 : %d\n",fChanEnaTrig[32]);
}

void HVON()
{
	t->dstmac[5] = 0xff; //Broadcast
	t->SendCMD(t->dstmac, FEB_GEN_HVON, 0x0000, buf);
}

void HVOF()
{
	t->dstmac[5] = 0xff; //Broadcast
	t->SendCMD(t->dstmac, FEB_GEN_HVOF, 0x0000, buf);
}


UInt_t GrayToBin(UInt_t n)
{
	UInt_t res = 0;
	int a[32], b[32], i = 0, c = 0;

	for (i = 0; i < 32; i++) {
		if ((n & 0x80000000) > 0) a[i] = 1;
		else a[i] = 0;
		n = n << 1;
	}

	b[0] = a[0];
	for (i = 1; i < 32; i++) {
			if (a[i] > 0) if (b[i - 1] == 0) b[i] = 1; else b[i] = 0;
		else b[i] = b[i - 1];
	}

	for (i = 0; i < 32; i++) {
		res = (res << 1);
		res = (res | b[i]);
	}

	return res;
}


void FillHistos(int truncat)  // hook called by libFEBDTP when event is received
{
  //printf("Calling fill histograms\n");
  int jj;
  int kk;
  UShort_t adc;
  int evspack = 0;
  UInt_t tt0, tt1;
  UChar_t ls2b0, ls2b1; //least sig 2 bits
  
  jj = 0;
  while (jj < truncat - 18)
    {
      overwritten = *(UShort_t*)(&(t->gpkt).Data[jj]);
      jj = jj + 2;
      lostinfpga = *(UShort_t*)(&(t->gpkt).Data[jj]);
      jj = jj + 2;
      ts0 = *(UInt_t*)(&(t->gpkt).Data[jj]); jj = jj + 4;
      ts1 = *(UInt_t*)(&(t->gpkt).Data[jj]); jj = jj + 4;
      
      ls2b0 = ts0 & 0x00000003;
      ls2b1 = ts1 & 0x00000003;
      tt0 = (ts0 & 0x3fffffff) >> 2;
      tt1 = (ts1 & 0x3fffffff) >> 2;
      tt0 = (GrayToBin(tt0) << 2) | ls2b0;
      tt1 = (GrayToBin(tt1) << 2) | ls2b1;
      tt0 = tt0 + 5; //IK: correction based on phase drift w.r.t GPS
      tt1 = tt1 + 5; //IK: correction based on phase drift w.r.t GPS
      NOts0 = ((ts0 & 0x40000000) > 0); // check overflow bit
      NOts1 = ((ts1 & 0x40000000) > 0);
      if ((ts0 & 0x80000000) > 0) {
	ts0 = 0x0; ts0_ref = tt0; ts0_ref_MEM[t->dstmac[5]] = tt0;
	ts0_ref_AVE[t->dstmac[5]] = ts0_ref_AVE[t->dstmac[5]] + ts0_ref_MEM[t->dstmac[5]]; (ts0_ref_IND[t->dstmac[5]])++;
      }
      else { ts0 = tt0; ts0_ref = ts0_ref_MEM[t->dstmac[5]]; }
      if ((ts1 & 0x80000000) > 0) {ts1 = 0x0; ts1_ref = tt1; ts1_ref_MEM[t->dstmac[5]] = tt1;} else { ts1 = tt1; ts1_ref = ts1_ref_MEM[t->dstmac[5]]; }
      
      //printf("T0=%u ns, T1=%u ns T0_ref=%u ns  T1_ref=%u ns \n", ts0, ts1, ts0_ref, ts1_ref);
      //printf(" ADC[32]:\n");
      
      printf("EVENT %d ",evs);
      printf("%u %u ",ts0, ts1);
      for (kk = 0; kk < 32; kk++)
	if (CHAN_MASK & (1 << kk))
	  {
	    adc = *(UShort_t*)(&(t->gpkt).Data[jj]); jj++; jj++;
	    printf("%04u ", adc);
	    chg[kk] = adc;
	  }
	else {chg[kk] = 0; jj += 2;}
      
      // Save the output TTree and update histograms
      printf("\n");


      mac5 = t->dstmac[5];
      if (t->dstmac[5] == t->macs[BoardToMon][5])
	{
	  evs++;
	  evspack++;
	  evsperrequest++;
	  total_lost += lostinfpga;
	  ts0_ref_mon = ts0_ref;
	  ts1_ref_mon = ts1_ref;
	  NOts0_mon = NOts0;
	  NOts1_mon = NOts1;
	}
    }
}


void ConfigSetBit(UChar_t *buffer, UShort_t bitlen, UShort_t bit_index, Bool_t value)
{
	UChar_t byte;
	UChar_t mask;
	byte = buffer[(bitlen - 1 - bit_index) / 8];
	mask = 1 << (7 - bit_index % 8);
	byte = byte & (~mask);
	if (value) byte = byte | mask;
	buffer[(bitlen - 1 - bit_index) / 8] = byte;
}

void SetDstMacByIndex(int i)
{
	if (i >= t->nclients || i < 0) return;
	for (int j = 0; j < 6; j++) t->dstmac[j] = t->macs[i][j];
	mac5 = t->macs[i][5];
}

UChar_t ConfigGetGain(int chan)
{
	UChar_t val = 0;
	for (int b = 0; b < 6; b++)
	{
		val = val << 1;
		if (ConfigGetBit(bufSCR, 1144, 619 + chan * 15 + b)) val = val + 1;
	}
	return val;
}

UChar_t ConfigGetBias(int chan)
{
	UChar_t val = 0;
	for (int b = 0; b < 8; b++)
	{
		val = val << 1;
		if (ConfigGetBit(bufSCR, 1144, 331 + chan * 9 + b)) val = val + 1;
	}
	return val;
}

void ConfigSetGain(int chan, UChar_t val)
{
	UChar_t mask = 1 << 5;
	for (int b = 0; b < 6; b++)
	{
		if ((val & mask) > 0) ConfigSetBit(bufSCR, 1144, 619 + chan * 15 + b, kTRUE); else ConfigSetBit(bufSCR, 1144, 619 + chan * 15 + b, kFALSE);
		mask = mask >> 1;
	}

}

void ConfigSetBias(int chan, UChar_t val)
{
	UChar_t mask = 1 << 7;
	for (int b = 0; b < 8; b++)
	{
		if ((val & mask) > 0) ConfigSetBit(bufSCR, 1144, 331 + chan * 9 + b, kTRUE); else ConfigSetBit(bufSCR, 1144, 331 + chan * 9 + b, kFALSE);
		mask = mask >> 1;
	}

}



void PrintConfig(UChar_t *buffer, UShort_t bitlen)
{
	UChar_t byte;
	UChar_t mask;
	for (int i = 0; i < bitlen; i++)
	{
		byte = buffer[(bitlen - 1 - i) / 8];
		mask = 1 << (7 - i % 8);
		byte = byte & mask;
		if (byte == 0) printf("0"); else printf("1");
	}
	printf("\n");
}

void SendConfig()
{
	uint32_t trigmask = 0;
	uint8_t bufFIL[256];

	for (int i = 265; i < 265 + 32; i++)
			if (fChanEnaTrig[i - 265] > 0) ConfigSetBit(bufSCR, 1144, i, 1); else  ConfigSetBit(bufSCR, 1144, i, 0);

	if (fChanEnaTrig[32] > 0)
		ConfigSetBit(bufSCR, 1144, 1139, 1); else  ConfigSetBit(bufSCR, 1144, 1139, 1); //OR32 enable

	for (int i = 0; i < 32; i++)
		if (fChanEnaAmp[i] > 0)
				ConfigSetBit(bufSCR, 1144, 633 + i * 15, 0); else  ConfigSetBit(bufSCR, 1144, 633 + i * 15, 1);

	for (int i = 0; i < 32; i++)
		if (fChanProbe[i] > 0)
				ConfigSetBit(bufPMR, 224, 96 + i, 1); else  ConfigSetBit(bufPMR, 224, 96 + i, 0);

	for (int i = 0; i < 32; i++) ConfigSetBias(i, fChanBias[i]);
	for (int i = 0; i < 32; i++) ConfigSetGain(i, fChanGain[i]);

	for (int i = 0; i < 32; i++)
		if (fChanEnaTrig[i] > 0) trigmask = trigmask | (0x1 << i);
	*((uint32_t*)(&(bufFIL[0]))) = trigmask;

	t->dstmac[5] = 0xff; //Broadcast
	t->SendCMD(t->dstmac, FEB_WR_SCR, 0x0000, bufSCR);
	t->SendCMD(t->dstmac, FEB_WR_PMR, 0x0000, bufPMR);
	t->SendCMD(t->dstmac, FEB_WR_FIL, 0x0000, bufFIL);
}

void SendAllChecked()
{
	for (int i = 0; i < 32; i++)
	{
		fChanEnaAmp[i] = 1;
		ConfigSetBit(bufSCR, 1144, 633 + i * 15, 0);
	}

	SetDstMacByIndex(BoardToMon);
	t->SendCMD(t->dstmac, FEB_WR_SCR, 0x0000, bufSCR);
	fChanEnaAmp[33] = 0; fChanEnaAmp[32] = 0;
}
void SendAllUnChecked()
{
	for (int i = 0; i < 32; i++)
	{
		fChanEnaAmp[i] = 0;
		ConfigSetBit(bufSCR, 1144, 633 + i * 15, 1);
	}

	SetDstMacByIndex(BoardToMon);
	t->SendCMD(t->dstmac, FEB_WR_SCR, 0x0000, bufSCR);
	fChanEnaAmp[33] = 0; fChanEnaAmp[32] = 0;
}


void UpdateHisto()
{
	// Place to draw the boards

}

void StopDAQ()
{
	t->dstmac[5] = 0xff; //Broadcast
	t->SendCMD(t->dstmac, FEB_GEN_INIT, 0, buf);
}


void StartDAQ(int nev = 0)
{
	t->dstmac[5] = 0xff; //Broadcast
	t->SendCMD(t->dstmac, FEB_GEN_INIT, 1, buf); //reset buffer
	t->SendCMD(t->dstmac, FEB_GEN_INIT, 2, buf); //set DAQ_Enable flag on FEB
	RunOn = 1;
	DAQ(nev);
	RunOn = 0;
	StopDAQ();
}


void DAQ(int nev = 0)
{
	char str1[32];
	char str2[32];
	evs = 0;
	int nevv = nev;
	double avts0;
	int deltaVCXO;
	int notok = 0;
	int ok = 0;
	total_lost = 0;
	RunOn = 1;
	float PollPeriod;
	float rate;
	tm0 = time(NULL);
	tm1 = time(NULL);
        long lasttm = 0;
bool donecorrection = 0;
	while (RunOn == 1 && (nevv == 0 || evs < nevv))
	{
        //printf("Starting daq\n");
	  
	  //Perform VCXO correction
          if (!donecorrection){
	  for (int feb = 0; feb < t->nclients; feb++)
	    {
	      if (ts0_ref_IND[t->macs[feb][5]] >= 20)
		{	//correct one FEB VCXO
		  avts0 = ts0_ref_AVE[t->macs[feb][5]] / ts0_ref_IND[t->macs[feb][5]];
		  deltaVCXO = -(avts0 - 1e9) / 5.2; //derive correction increment, approx 5.2 ns per DAC LSB
		  VCXO_Values[feb] = VCXO_Values[feb] + deltaVCXO;
		  ts0_ref_AVE[t->macs[feb][5]] = 0; ts0_ref_IND[t->macs[feb][5]] = 0;
		}
	    }
          donecorrection = 1;
	  }
	  chan = 0; //fNumberEntry886->GetNumber();
	  rate = GetTriggerRate() / 1e3;
          if (tm1 != lasttm){
	  printf("Trigger %2.3f kHz after %d seconds %d \n", rate, tm1-tm0);
lasttm = time(NULL);
	  }
	  for (int feb = 0; feb < t->nclients; feb++)
	    {
	      SetDstMacByIndex(feb);
	      mac5 = t->dstmac[5];
	      ok = t->SendCMD(t->dstmac, FEB_RD_CDR, 0, buf);
	    }
	  
	  //printf("Per request: %d events acquired, overwritten (flags field of last event) %d\n", evsperrequest, overwritten);
	  //if (nevv > 0) printf("%d events to go...\n", nevv - evs);
	  
	  total_lost += overwritten;
	  overwritten = 0;
	  evsperrequest = 0;
	  tm1 = time(NULL);	  
	}
	//printf("Overal per DAQ call: %d events acquired, %d (%2.1f\%%) lost (skipping first request).\n", evs, total_lost, (100.*total_lost / (evs + total_lost)));
}

void Reset()
{
	evs = 0;
	total_lost = 0;
}

float GetTriggerRate()
{
	float retval;
	t->SendCMD(t->macs[BoardToMon], FEB_GET_RATE, 0, buf);
	retval = *((float*)(t->gpkt.Data));
	return retval;
}

void SetThresholdDAC1(UShort_t dac1)
{
	int offset = 1107;
	for (int i = 0; i < 10; i++)
	{
		if ( (dac1 & 1) > 0) ConfigSetBit(bufSCR, 1144, offset + 9 - i, kTRUE);
		else ConfigSetBit(bufSCR, 1144, offset + 9 - i, kFALSE);
		dac1 = dac1 >> 1;
	}
	t->SendCMD(t->dstmac, FEB_WR_SCR, 0x0000, bufSCR);
}

UShort_t GetThresholdDAC1()
{
	int offset = 1107;
	UShort_t dac1 = 0;
	for (int i = 0; i < 10; i++)
	{
		dac1 = dac1 >> 1;
		if (ConfigGetBit(bufSCR, 1144, offset + 9 - i)) dac1 = dac1 | 0x0200;
	}
	return dac1;
}

void SetThresholdDAC2(UShort_t dac1)
{
	int offset = 1117;
	for (int i = 0; i < 10; i++)
	{
		if ( (dac1 & 1) > 0) ConfigSetBit(bufSCR, 1144, offset + 9 - i, kTRUE);
		else ConfigSetBit(bufSCR, 1144, offset + 9 - i, kFALSE);
		dac1 = dac1 >> 1;
	}
	t->SendCMD(t->dstmac, FEB_WR_SCR, 0x0000, bufSCR);
}


/*
void GUI_UpdateThreshold() 
{
	UShort_t dac1;
	dac1 = DACTHRESHOLD; //fNumberEntry755->GetNumber();
	t->dstmac[5] = 0xff;
	SetThresholdDAC1(dac1);
	SetThresholdDAC2(dac1);
}

void GUI_UpdateVCXO() // DONE
{
	t->VCXO = 500; //fNumberEntry75->GetNumber();
	int temp = t->VCXO;
	t->SendCMD(t->macs[BoardToMon], FEB_SET_RECV, temp, t->srcmac);
}
*/

void UpdateVCXOAllFEBs() // DONE
{
	for (int feb = 0; feb < t->nclients; feb++)   t->SendCMD(t->macs[feb], FEB_SET_RECV, VCXO_Values[feb], t->srcmac);
}

void UpdateBoardMonitor() // DONE
{
	BoardToMon = 0; //fNumberEntry8869->GetNumber();
	char sttr[32];
	sprintf(sttr, "Mon FEB 0x%2x %d", t->macs[BoardToMon][5], t->macs[BoardToMon][5]);
	printf("Monitoring FEB mac5 0x%2x %d\n", t->macs[BoardToMon][5], t->macs[BoardToMon][5]);
}
